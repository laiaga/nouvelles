# What this is about

Short stories I've been writing for some time now for fun. They are all on paper and I intend to latexify them little by little. Also, it's written in french.

## Les drôles d'êtres de lumière 

*The weird people of light*

Freely inspired by [Dark City](https://www.imdb.com/title/tt0118929/) and [Lyon Festival of Lights](https://www.fetedeslumieres.lyon.fr/en).

The story is set in a world where the sun hasn't risen in a long, long time, and somehow nobody notices except for the heroine's husband. More and more troubled by this fact, he ends up eloping and the heroines follows him and tries to catch him through this weird, dark city. 

## Y'a d'la joie

*There is joy*

(Very) freely inspired by Charles Trenet's song [Y'a d'la joie](https://www.youtube.com/watch?v=ae9AQayZAzA).

It is a very short story about a veteran overwhelmed by PTSD some time after WW1.

## Qui a volé les étoiles ?

*Whole stole the stars?*

Not yet digitized, this is the next one to come. A black detective story set in the 30s, in New-York: a weird woman tasks our hero with finding who steals the stars from the sky, night after night. Quite sceptical at first, he accepts the case for the money only... and ends up actually tracking a star thief across the city. 

